package hola.example.appmenubuttom92

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import hola.example.appmenubuttom92.R

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class HomeFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val facebookIcon: ImageView = view.findViewById(R.id.facebook_icon)
        val twitterIcon: ImageView = view.findViewById(R.id.twitter_icon)
        val instagramIcon: ImageView = view.findViewById(R.id.instagram_icon)
        val linkedinIcon: ImageView = view.findViewById(R.id.linkedin_icon)

        facebookIcon.setOnClickListener {
            openUrl("https://www.facebook.com/profile.php?id=100009052815901&mibextid=ZbWKwL")
        }

        twitterIcon.setOnClickListener {
            openUrl("https://www.twitter.com")
        }

        instagramIcon.setOnClickListener {
            openUrl("https://www.instagram.com/jluis_ram23/")
        }

        linkedinIcon.setOnClickListener {
            openUrl("linkedin.com/in/JluisRM")
        }

        return view
    }

    private fun openUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
